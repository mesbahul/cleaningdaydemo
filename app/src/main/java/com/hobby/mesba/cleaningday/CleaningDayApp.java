package com.hobby.mesba.cleaningday;

import android.app.Application;

import com.hobby.mesba.cleaningday.dagger.AppComponent;
import com.hobby.mesba.cleaningday.dagger.DaggerAppComponent;
import com.hobby.mesba.cleaningday.dagger.modules.AppModule;
import com.hobby.mesba.cleaningday.dagger.modules.BackendModule;

/**
 * Created by Mesbahul Islam on 1/28/2017.
 */

public class CleaningDayApp extends Application {
    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .backendModule(new BackendModule("http://165.227.145.98:3000/"))
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
