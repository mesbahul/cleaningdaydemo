package com.hobby.mesba.cleaningday.main;

import android.content.Context;
import android.content.Intent;

import com.hobby.mesba.cleaningday.backend.BackendClient;
import com.hobby.mesba.cleaningday.models.Shop;
import com.hobby.mesba.cleaningday.shoplist.ShopListViewActivity;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Mesbahul Islam on 1/28/2017.
 */

public class MainActivityPresenter implements MainActivityContract.Presenter {
    private final Context context;
    private final MainActivityContract.View view;
    private final BackendClient client;

    @Inject
    public MainActivityPresenter(Context context, MainActivityContract.View view, BackendClient client) {
        this.context = context;
        this.view = view;
        this.client = client;
    }

    @Override
    public void loadShops() {
        client.getAllShops()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<Shop>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull List<Shop> shops) {
                        view.showShops(shops);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        view.showError(e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        view.showComplete();
                    }
                });
    }

    @Override
    public void showShopListActivity() {
        Intent intent = new Intent(context, ShopListViewActivity.class);
        context.startActivity(intent);
    }
}
