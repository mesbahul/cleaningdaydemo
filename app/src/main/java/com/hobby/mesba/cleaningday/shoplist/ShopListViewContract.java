package com.hobby.mesba.cleaningday.shoplist;

import com.hobby.mesba.cleaningday.models.Shop;

import java.util.List;

/**
 * Created by Mesbahul Islam on 7/22/2017.
 */

public interface ShopListViewContract {
    interface View {
        void showShops(List<Shop> shops);

        void showError(String message);

        void showComplete();
    }

    interface Presenter {
        void loadShops();
    }
}
