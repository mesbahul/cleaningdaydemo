package com.hobby.mesba.cleaningday.dagger;

import android.content.Context;

import com.hobby.mesba.cleaningday.backend.BackendApi;
import com.hobby.mesba.cleaningday.dagger.modules.AppModule;
import com.hobby.mesba.cleaningday.dagger.modules.BackendModule;
import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Mesbahul Islam on 1/28/2017.
 */

@Singleton
@Component(modules = {AppModule.class, BackendModule.class})
public interface AppComponent {
    BackendApi api();

    Context context();

    Picasso picasso();
}
