package com.hobby.mesba.cleaningday.backend;

import com.hobby.mesba.cleaningday.models.Shop;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Mesbahul Islam on 1/28/2017.
 */

public interface BackendApi {
    @GET("shops")
    Observable<List<Shop>> getShopList();

    @GET("shops")
    Observable<List<Shop>> paginateShops(@Query("_page") int page);
}
