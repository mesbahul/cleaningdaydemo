package com.hobby.mesba.cleaningday.shoplist;

import android.content.Context;

import com.hobby.mesba.cleaningday.backend.BackendClient;
import com.hobby.mesba.cleaningday.models.Shop;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Mesbahul Islam on 7/22/2017.
 */

public class ShopListViewPresenter implements ShopListViewContract.Presenter {
    private final Context context;
    private final ShopListViewContract.View view;
    private final BackendClient client;

    @Inject
    public ShopListViewPresenter(Context context, ShopListViewContract.View view, BackendClient client) {
        this.context = context;
        this.view = view;
        this.client = client;
    }

    @Override
    public void loadShops() {
        client.getAllShops()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<List<Shop>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull List<Shop> shops) {
                        view.showShops(shops);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        view.showError(e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        view.showComplete();
                    }
                });
    }
}
