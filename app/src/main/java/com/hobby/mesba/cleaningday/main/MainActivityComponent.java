package com.hobby.mesba.cleaningday.main;

import com.hobby.mesba.cleaningday.dagger.AppComponent;
import com.hobby.mesba.cleaningday.dagger.CustomScope;

import dagger.Component;

/**
 * Created by Mesbahul Islam on 1/28/2017.
 */

@CustomScope
@Component(dependencies = AppComponent.class, modules = MainActivityModule.class)
public interface MainActivityComponent {
    void inject(MainActivity activity);
}
