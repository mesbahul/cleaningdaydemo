package com.hobby.mesba.cleaningday.shoplist;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hobby.mesba.cleaningday.R;
import com.hobby.mesba.cleaningday.models.Shop;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mesbahul Islam on 7/22/2017.
 */

public class ShopsAdapter extends RecyclerView.Adapter<ShopsAdapter.ShopItemViewHolder> {
    private final Context context;
    private final LayoutInflater inflater;
    private final Callback callback;
    private final List<Shop> shops;
    private final Picasso picasso;

    public ShopsAdapter(Context context, Callback callback, List<Shop> shops, Picasso picasso) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.callback = callback;
        this.shops = shops;
        this.picasso = picasso;
    }

    @Override
    public ShopItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return ShopItemViewHolder.inflate(inflater, parent);
    }

    @Override
    public void onBindViewHolder(ShopItemViewHolder holder, int position) {
        Shop shopItem = shops.get(position);

        holder.shopName.setText(shopItem.getName());
        holder.shopAddress.setText(shopItem.getAddress());

        picasso.load(shopItem.getPicture())
                .resize(1000, 400)
                .centerCrop()
                .into(holder.shopImage);

        holder.cardView.setOnClickListener(v -> callback.onShopItemClicked(shopItem.getId()));
    }

    @Override
    public int getItemCount() {
        return shops.size();
    }

    public interface Callback {
        void onShopItemClicked(Integer shopId);
    }

    static class ShopItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.shop_item)
        CardView cardView;

        @BindView(R.id.shop_image)
        ImageView shopImage;

        @BindView(R.id.shop_name)
        TextView shopName;

        @BindView(R.id.shop_address)
        TextView shopAddress;

        public ShopItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        static ShopItemViewHolder inflate(LayoutInflater inflater, ViewGroup parent) {
            View view = inflater.inflate(R.layout.layout_shop_item, parent, false);
            return new ShopItemViewHolder(view);
        }
    }
}
