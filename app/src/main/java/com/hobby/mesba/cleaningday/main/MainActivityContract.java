package com.hobby.mesba.cleaningday.main;

import com.hobby.mesba.cleaningday.models.Shop;

import java.util.List;

/**
 * Created by Mesbahul Islam on 1/28/2017.
 */

public interface MainActivityContract {
    interface View {
        void showShops(List<Shop> shops);

        void showError(String message);

        void showComplete();
    }

    interface Presenter {
        void loadShops();

        void showShopListActivity();
    }
}
