package com.hobby.mesba.cleaningday.main;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.hobby.mesba.cleaningday.BaseActivity;
import com.hobby.mesba.cleaningday.R;
import com.hobby.mesba.cleaningday.models.Shop;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class MainActivity extends BaseActivity implements MainActivityContract.View {

    @BindView(R.id.textView_display) TextView displayText;
    @BindView(R.id.button_fetch) Button fetchButton;

    @Inject MainActivityPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DaggerMainActivityComponent.builder()
                .appComponent(getAppComponent())
                .mainActivityModule(new MainActivityModule(this))
                .build().inject(this);
    }

    @Override
    public void showShops(List<Shop> shops) {
        displayText.setText("Number of Shops " + shops.size());
    }

    @Override
    public void showError(String message) {
        //Show error message Toast
        Toast.makeText(getApplicationContext(), "Error " + message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showComplete() {
        //Show completed message Toast
        Toast.makeText(getApplicationContext(), "Complete", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.button_fetch)
    void onFetchButtonClick() {
        presenter.loadShops();
    }

    @OnClick(R.id.button_list)
    void onShowListButtonClicked() {
        presenter.showShopListActivity();
    }
}
