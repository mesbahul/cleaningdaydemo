package com.hobby.mesba.cleaningday.backend;

import com.hobby.mesba.cleaningday.models.Shop;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by Mesbahul Islam on 7/20/2017.
 */

public class BackendClient {
    private final BackendApi api;

    @Inject
    public BackendClient(BackendApi api) {
        this.api = api;
    }

    public Observable<List<Shop>> getAllShops() {
        return api.getShopList();
    }
}
