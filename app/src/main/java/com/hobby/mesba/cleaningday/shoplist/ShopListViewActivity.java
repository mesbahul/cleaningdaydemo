package com.hobby.mesba.cleaningday.shoplist;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.hobby.mesba.cleaningday.BaseActivity;
import com.hobby.mesba.cleaningday.R;
import com.hobby.mesba.cleaningday.dagger.AppComponent;
import com.hobby.mesba.cleaningday.dagger.CustomScope;
import com.hobby.mesba.cleaningday.models.Shop;
import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import dagger.Component;
import dagger.Module;
import dagger.Provides;

public class ShopListViewActivity
        extends BaseActivity
        implements ShopListViewContract.View, ShopsAdapter.Callback {

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @Inject
    ShopListViewPresenter presenter;

    @Inject
    Picasso picasso;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_list_view);

        DaggerShopListViewActivity_ShopListViewActivityComponent.builder()
                .appComponent(getAppComponent())
                .shopListViewActivityModule(new ShopListViewActivityModule(this))
                .build().inject(this);

        presenter.loadShops();
    }

    @Override
    public void showShops(List<Shop> shops) {
        LinearLayoutManager llm = new LinearLayoutManager(this);
        ShopsAdapter shopsAdapter = new ShopsAdapter(this, this, shops, picasso);
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(shopsAdapter);
    }

    @Override
    public void showError(String message) {
        Toast.makeText(getApplicationContext(), "Loading Error " + message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showComplete() {
        Toast.makeText(getApplicationContext(), "Loading Completed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onShopItemClicked(Integer shopId) {
        Toast.makeText(getApplicationContext(), "Open Shop Details of " + shopId, Toast.LENGTH_SHORT).show();
    }

    @CustomScope
    @Component(dependencies = AppComponent.class, modules = ShopListViewActivity.ShopListViewActivityModule.class)
    public interface ShopListViewActivityComponent {
        void inject(ShopListViewActivity activity);
    }

    @Module
    public static class ShopListViewActivityModule {
        private final ShopListViewContract.View view;

        public ShopListViewActivityModule(ShopListViewContract.View view) {
            this.view = view;
        }

        @Provides
        ShopListViewContract.View view() {
            return view;
        }
    }
}
