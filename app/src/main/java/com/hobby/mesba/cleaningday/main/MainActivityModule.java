package com.hobby.mesba.cleaningday.main;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Mesbahul Islam on 1/28/2017.
 */

@Module
public class MainActivityModule {
    private final MainActivityContract.View view;

    public MainActivityModule(MainActivityContract.View view) {
        this.view = view;
    }

    @Provides
    MainActivityContract.View view() {
        return view;
    }
}
